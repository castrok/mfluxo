<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CartProductController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'products' => ProductController::class,
    'carts' => CartController::class,
    'shopping' => CartProductController::class,
]);

Route::get('thumbnails/{document}', function ($document) {
    return Response::file("storage/thumbnails/$document");
});
