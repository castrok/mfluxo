<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cart
 *
 * @property int $id
 * @property string $situation
 * @property string $name
 * @property timestamp $created_at
 * @property timestamp|null $updated_at
 *
 */
class Cart extends Model {

    protected $table = 'carts';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'situation',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<string>
     */
    protected $hidden = [
        'pivot',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d h:i',
        'updated_at' => 'datetime:Y-m-d h:i'
    ];

    public function products() {
        return $this->belongsToMany(Product::class, 'cart_products', 'fk_cart', 'fk_product');
    }
}
