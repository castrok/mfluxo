<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @property int $id
 * @property int $quantity
 * @property int $fk_created_at
 * @property timestamp $created_at
 *
 */
class CartProduct extends Model
{
    public $timestamps = false;

    protected $table = 'cart_products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'fk_cart',
        'fk_product',
        'quantity',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<string>
     */
    protected $hidden = [
        'pivot',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d h:i',
    ];

    public function cart() {
        return $this->belongsTo(Cart::class, 'fk_cart');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'fk_product');
    }
}
