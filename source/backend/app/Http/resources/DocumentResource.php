<?php

namespace App\Http\resources;

use Illuminate\Support\Facades\Storage;

class DocumentResource {
    public static function handleDocument($document): string {
        $documentName = "";
        if ($document) {
            //OBTEM A EXTENSÃO
            $extension = explode('/', $document);
            $extension = explode(';', $extension[1])[0];

            //OBTEM O ARQUIVO
            $separatorFile = explode(',', $document);
            $document = $separatorFile[1];

            $name = uniqid(date('HisYmd'));

            //ENVIA O ARQUIVO
            $documentName = "{$name}.{$extension}";
            Storage::put("public/thumbnails/$documentName", base64_decode($document));
        }
        return $documentName;
    }
}
