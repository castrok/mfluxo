<?php

namespace App\Http\Controllers;

use App\Http\Repository\CartRepository;
use App\Http\Requests\CartRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CartController extends Controller
{

    public function __construct(protected CartRepository $repository)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $response = $this->repository->index();
        return Response::json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CartRequest $request
     * @return JsonResponse
     */
    public function store(CartRequest $request): JsonResponse {
        $response = $this->repository->store($request);
        return Response::json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse {
        $response = $this->repository->show($id);
        return Response::json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CartRequest $request
     * @param string $id
     * @return JsonResponse
     */
    public function update(CartRequest $request, string $id): JsonResponse {
        $response = $this->repository->update($request, $id);
        return Response::json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse {
        $response = ['success' => $this->repository->destroy($id)];
        return Response::json($response);
    }
}
