<?php

namespace App\Http\Repository;

use App\Http\Interfaces\RepositoryInterface;
use App\Models\Cart;
use Illuminate\Database\Eloquent\Collection;

class CartRepository implements RepositoryInterface {

    public function index(): Collection {
        return Cart::all();
    }

    public function store($request): Cart {
        $newCart = $request->validated();
        return Cart::create($newCart)->fresh();
    }

    public function update($request, $id): Cart {
        $cart = Cart::find($id);
        $updatedCart = $request->validated();
        $cart->update($updatedCart);
        return $cart;
    }

    public function show($id): Collection {
        return Cart::with('products')->where('id', $id)->get();
    }

    public function destroy($id): int {
        return Cart::destroy($id);
    }
}
