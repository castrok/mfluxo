<?php

namespace App\Http\Repository;

use App\Http\Interfaces\RepositoryInterface;
use App\Models\CartProduct;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CartProductRepository implements RepositoryInterface {

    public function index(): Collection {
        return CartProduct::with(['cart', 'product'])->get();
    }

    public function store($request): CartProduct
    {
        $newProduct = $request->validated();
        return CartProduct::create($newProduct)->fresh();
    }

    public function update($request, $id) {
        $cartProduct = CartProduct::find($id);
        $updatedCart = $request->validated();
        $cartProduct->update($updatedCart);
        return CartProduct::with(['cart', 'product'])->where('id', $id)->first();
    }

    public function show($id): CartProduct
    {
        return CartProduct::find($id);
    }

    public function destroy($id): int
    {
        return CartProduct::destroy($id);
    }
}
