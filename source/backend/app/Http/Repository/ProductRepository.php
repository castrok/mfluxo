<?php

namespace App\Http\Repository;

use App\Http\Interfaces\RepositoryInterface;
use App\Http\resources\DocumentResource;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class ProductRepository implements RepositoryInterface
{

    public function index(): Collection
    {
        return Product::all();
    }

    public function store($request): Product
    {
        $newProduct = $request->validated();
        $document = $request->safe()->only('thumbnail')['thumbnail'] ?? null;
        $newProduct['thumbnail'] = DocumentResource::handleDocument($document);

        return Product::create($newProduct)->fresh();
    }

    public function update($request, $id): Product
    {
        $cart = Product::find($id);
        $updatedProduct = $request->validated();

        $document = $request->safe()->only('thumbnail')['thumbnail'] ?? null;
        Log::info($document);
        if (!$document)
            unset($updatedProduct['thumbnail']);
        else
            $updatedProduct['thumbnail'] =DocumentResource::handleDocument($document);

        $cart->update($updatedProduct);
        return $cart;
    }

    public function show($id): Product
    {
        return Product::find($id);
    }

    public function destroy($id): int
    {
        return Product::destroy($id);
    }
}
