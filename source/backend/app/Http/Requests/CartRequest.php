<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CartRequest extends FormRequest
{
    protected $stopOnFirstFailure = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'string|nullable|max:254',
            'situation' => [
                Rule::in('BUYING', 'CLOSED')
            ],
        ];
    }

    public function attributes(): array {
        return [
            'name' => 'Nome',
            'Situação' => 'situation',
        ];
    }
}
