<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class ProductRequest extends FormRequest
{
    /**
     * Indicates if the validator should stop on the first rule failure.
     * @var bool
     */
    protected $stopOnFirstFailure = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validators = [
            'thumbnail' => 'base64image|base64mimes:jpg,jpeg,png',
            'value' => '',
            'description' => 'string|nullable',
            'name' => 'string|max:254'
        ];

        if (empty($this->route('product'))) {
            $validators = [
                'thumbnail' => 'required|base64image|base64mimes:jpg,jpeg,png',
                'value' => 'required',
                'description' => 'string|nullable',
                'name' => 'string|required|max:254',
            ];
        }
        return $validators;
    }

    public function attributes(): array {
        return [
            'value' => 'Valor',
            'thumbnail' => 'Imagem',
            'name' => 'Nome',
            'descrição' => 'Descrição',
        ];
    }
}
