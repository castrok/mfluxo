<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartProductRequest extends FormRequest
{
    protected $stopOnFirstFailure = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $validators = [
            'quantity' => "numeric|min:1",
        ];
        if (empty($this->route('shopping'))) {
            $validators = [
                'fk_cart' => "required|exists:carts,id|numeric",
                'fk_product' => "required|exists:products,id|numeric",
                'quantity' => "required|numeric|min:1",
            ];
        }
        return $validators;
    }

    public function attributes(): array
    {
        return [
            'fk_cart' => 'Carrinho',
            'fk_product' => 'Produto',
            'quantity' => 'Quantidade',
        ];
    }
}
