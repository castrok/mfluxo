export const config = {
  production: false,
  prefix: '/api',
  protocol: 'http://',
  hosts: {
    base: location.host,
  }
};
export const baseURL: string = config.protocol + config.hosts.base + config.prefix
export const environment = {
  carts: baseURL + '/carts',
  products: baseURL + '/products',
  shopping: baseURL + '/shopping',
};
