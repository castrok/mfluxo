import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CartProductService} from "../../../shared/services/cart-product.service";
import CartProductInterface from "../../../shared/interfaces/cart-product-interface";
import {subTotal} from "../../../shared/providers/total-cart-product-provider";
import CartInterface from "../../../shared/interfaces/cart-interface";
import StorageProvider from "../../../shared/providers/storage.provider";
import {ToastrService} from "ngx-toastr";
import {CartService} from "../../../shared/services/cart.service";

@Component({
  selector: 'mfluxo-carts-detail',
  templateUrl: './carts-detail.component.html',
  styleUrls: ['./carts-detail.component.scss']
})
export class CartsDetailComponent implements OnInit {

  cartProducts: CartProductInterface[] = Object.assign([])
  cart: CartInterface = Object.assign({})
  protected readonly subTotal = subTotal;

  constructor(
      private activatedRoute: ActivatedRoute,
      private cartProductService: CartProductService,
      private cartService: CartService,
      private storageProvider: StorageProvider,
      private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.index()
    this.cart = this.storageProvider.decriptNdGetLocalStorage('currentCart')
  }

  updateQuantity(cartProduct: CartProductInterface, {target}: any): void {
    if (this.cart.situation === 'BUYING') {
      const id: string = String(cartProduct.id)
      this.cartProductService.update(id, {quantity: target.value} as CartProductInterface).subscribe({
        next: response => {
          cartProduct.quantity = target.value
          this.toastr.info("Quantidade atualizada com sucesso")
        },
        error: error => console.error(error),
      })
    }
    else {
      this.toastr.info("Compra finalizada, selecione outro carrinho.")
    }
  }

  removeProduct(cartProduct: CartProductInterface): void {
    const id: string = String(cartProduct.id)
    this.cartProductService.delete(id).subscribe({
      next: response => {
        const index = this.cartProducts.indexOf(cartProduct)
        this.cartProducts.splice(index, 1)
        this.toastr.info("Prduto removido.")
      },
      error: error => {
        this.toastr.error("Ocorreu um erro ao remover o produto.")
        console.error(error)
      },
    })
  }
  private index(): void {
    const success = (cartProducts: CartProductInterface[]) => {
      const id = this.activatedRoute.snapshot.params['id']
      this.cartProducts = cartProducts.filter(cartProduct => cartProduct.fk_cart == id)
    }
    const fail = (error: any) => console.error(error)

    this.cartProductService.index().subscribe({
      next: cart => success(cart),
      error: error => fail(error),
    })
  }

  total(discount: number = 0) {
    return this.cartProducts.reduce((total: any, cartProduct: any) => total + subTotal(cartProduct.quantity, cartProduct.product.value, discount), 0)
  }

  totalAdded(): number {
    return this.cartProducts.reduce((total: any, cartProduct: any) => total + cartProduct.quantity, 0)

  }

  numberOnly(event: any): void {
    const bypassKeys: string[] = [
      'ArrowLeft',
      'ArrowRight',
      'Backspace',
    ]

    if (bypassKeys.indexOf(event.key) === -1 && !event['key'].match("^[0-9]*$"))
      event.preventDefault()
  }

  finalize() {
    const id: string = String(this.cart.id)
    this.cartService.update(id, {situation: "CLOSED"} as CartInterface).subscribe({
      next: response => {
        this.toastr.info("Compra realizada com sucesso.")
        this.cart.situation = "CLOSED"
        this.storageProvider.encriptNdStoreLocalStorage('currentCart', this.cart)
      },
      error: error => console.error(error),
    })
  }
}
