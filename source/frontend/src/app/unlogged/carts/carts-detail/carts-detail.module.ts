import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartsDetailComponent } from './carts-detail.component';
import {RouterLink, RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    CartsDetailComponent
  ],
    imports: [
        CommonModule,
        RouterLink,
        RouterModule.forChild([
            {path: '', component: CartsDetailComponent}
        ]),
        FormsModule,
        ReactiveFormsModule
    ]
})
export class CartsDetailModule {
    constructor() {
    console.log('CartsDetailModule')
    }
}
