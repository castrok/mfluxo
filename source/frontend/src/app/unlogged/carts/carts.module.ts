import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        loadChildren: () => import('./carts-list/carts-list.module').then(m => m.CartsListModule),
    },
    ...['create', ':id/edit'].map((path: string) => (
        {
            path: path,
            loadChildren: () => import('./carts-form/carts-form.module').then(m => m.CartsFormModule),
        }
    )),
    {
        path: ':id',
        loadChildren: () => import('./carts-detail/carts-detail.module').then(m => m.CartsDetailModule),
    },
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ]
})
export class CartsModule {
}
