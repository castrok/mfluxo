import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartsFormComponent } from './carts-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    CartsFormComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: '', component: CartsFormComponent}
        ])
    ]
})
export class CartsFormModule { }
