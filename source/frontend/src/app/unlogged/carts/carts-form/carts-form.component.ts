import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {CartService} from "../../../shared/services/cart.service";
import {WhiteSpacesValidator} from "../../../shared/validators/white-spaces.validator";
import CartInterface from "../../../shared/interfaces/cart-interface";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'mfluxo-carts-form',
  templateUrl: './carts-form.component.html',
  styleUrls: ['./carts-form.component.scss']
})
export class CartsFormComponent implements OnInit {

  formGroup: FormGroup = this.formBuilder.group({
    id: [
      "",
      {
        validators: [],
        asyncValidators: [],
        updateOn: 'change',
      },
    ],
    name: [
      "",
      {
        validators: [Validators.required, WhiteSpacesValidator()],
        asyncValidators: [],
        updateOn: 'change',
      },
    ],
    situation: [
      "BUYING",
      {
        validators: [Validators.required, WhiteSpacesValidator()],
        asyncValidators: [],
        updateOn: 'change',
      },
    ],
  })

  getFormEntry(control: string): AbstractControl | null {
    return this.formGroup.get(control)
  }

  constructor(
      private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder,
      private cartService: CartService,
      private router: Router,
      private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot?.params['id']
    if (id)
      this.show(id)
  }

  submit(): void {
    const cart: CartInterface = this.formGroup.getRawValue()
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched()
      Object.keys(this.formGroup.controls).map(control => this.getFormEntry(control)?.markAsDirty())
    } else {
      cart.id === null
          ? this.store(cart)
          : this.update(cart)
    }
  }

  private store(cart: CartInterface): void {
    delete cart.id
    const success = (response: CartInterface) => {
      this.router.navigate(['unlogged', 'products'])
      this.toastr.info("Carrinho de compras criado com sucesso!")
      console.log(response)
      this.formGroup.reset()
    }
    const fail = (error: any) => {
      console.error(error)
      this.toastr.error("Erro ao criar o carrinho de compras!")
    }

    this.cartService.store(cart).subscribe({
      next: response => success(response),
      error: error => fail(error),
    })
  }

  private update(cart: CartInterface): void {
    const success = (response: CartInterface) => {
      console.log(response)
      this.router.navigate(['unlogged', 'carts', response.id])
      this.toastr.info("Carrinho de compras atualizado com sucesso!")
    }
    const fail = (error: any) => {
      console.error(error)
      this.toastr.error("Erro ao atualizar o carrinho de compras!")
    }

    const id: string = String(cart.id)
    delete cart.id
    this.cartService.update(id, cart).subscribe({
      next: response => success(response),
      error: error => fail(error),
    })
  }

  private show(id: string): void {
    const success = (cart: CartInterface) => this.formGroup.patchValue({
      name: cart.name,
      id: cart.id
    })
    const fail = (error: any) => console.error(error)

    this.cartService.show(id).subscribe({
      next: response => success(response),
      error: error => fail(error),
    })
  }
}
