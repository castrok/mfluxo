import {Component, OnInit} from '@angular/core';
import {CartService} from "../../../shared/services/cart.service";
import CartInterface from "../../../shared/interfaces/cart-interface";
import StorageProvider from "../../../shared/providers/storage.provider";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {reduce} from "rxjs";

@Component({
    selector: 'mfluxo-carts-list',
    templateUrl: './carts-list.component.html',
    styleUrls: ['./carts-list.component.scss']
})
export class CartsListComponent implements OnInit {

    carts: CartInterface[] = Object.assign([])

    constructor(
        private cartService: CartService,
        protected storageProvider: StorageProvider,
        private toastr: ToastrService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.index()
    }

    private index(): void {
        const success = (carts: CartInterface[]) => this.carts = carts
        const fail = (error: any) => console.error(error)

        this.cartService.index().subscribe({
            next: carts => success(carts),
            error: error => fail(error),
        })
    }

    normalizeSituation(situation?: "BUYING" | "CLOSED"): 'Fechado' | 'Disponível' | null {

        if (situation === 'BUYING')
            return 'Disponível'

        if (situation === 'CLOSED')
            return 'Fechado'

        return null
    }

    setCart(cart: CartInterface): void {
        if (cart.situation === 'BUYING') {
            this.storageProvider.encriptNdStoreLocalStorage('currentCart', cart)
            this.toastr.success('Carrinho selecionado com sucesso')
            this.router.navigateByUrl('unlogged/products')
        }
        else {
            this.toastr.success('Este carrinho não pode mais ser usado!')
        }
    }
}
