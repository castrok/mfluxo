import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import { CartsListComponent } from './carts-list.component';
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [
    CartsListComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: '', component: CartsListComponent}
        ]),
        MatTooltipModule
    ]
})
export class CartsListModule {
  constructor() {
    console.log("CartsListModule")
  }
}
