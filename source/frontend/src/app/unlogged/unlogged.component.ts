import { Component } from '@angular/core';

@Component({
  selector: 'mfluxo-unlogged',
  template: `
    <mfluxo-navbar></mfluxo-navbar>
    <div class="container p-3" id="main">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [
      `
        div[id='main'] {
          box-shadow: rgba(0, 0, 0, 0.16) 0 10px 36px 0, rgba(0, 0, 0, 0.06) 0 0 0 1px;
        }
      `
  ]
})
export class UnloggedComponent {

}
