import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {UnloggedComponent} from './unlogged.component';
import {NavbarModule} from "../shared/components/navbar/navbar.module";


const routes: Routes = [
    {
        path: '',
        component: UnloggedComponent,
        children: [
            {
                path: 'products',
                loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
            },
            {
                path: 'carts',
                loadChildren: () => import('./carts/carts.module').then(m => m.CartsModule),
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'carts'
            }
        ]
    },
]

@NgModule({
    declarations: [
        UnloggedComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        NavbarModule,
    ]
})
export class UnloggedModule {
    constructor() {
        console.log('UnloggedModule')
    }
}
