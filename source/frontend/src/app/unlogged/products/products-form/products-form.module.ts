import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsFormComponent } from './products-form.component';
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {NgxCurrencyModule} from "ngx-currency";
import {NgxDropzoneModule} from "ngx-dropzone";


@NgModule({
  declarations: [
    ProductsFormComponent,
  ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProductsFormComponent
            }
        ]),
        ReactiveFormsModule,
        NgxCurrencyModule,
        NgxDropzoneModule
    ]
})
export class ProductsFormModule { }
