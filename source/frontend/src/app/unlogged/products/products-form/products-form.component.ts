import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WhiteSpacesValidator} from "../../../shared/validators/white-spaces.validator";
import ProductInterface from "../../../shared/interfaces/product-interface";
import {ProductService} from "../../../shared/services/product.service";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'mfluxo-products-form',
    templateUrl: './products-form.component.html',
    styleUrls: ['./products-form.component.scss']
})
export class ProductsFormComponent implements OnInit {

    formGroup: FormGroup = this.formBuilder.group({
        id: [
            null,
            {
                validators: [],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
        name: [
            "",
            {
                validators: [Validators.required, WhiteSpacesValidator()],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
        description: [
            "",
            {
                validators: [WhiteSpacesValidator()],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
        value: [
            "",
            {
                validators: [Validators.required],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
        thumbnail: [
            null,
            {
                validators: [Validators.required],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
    })
    document: any

    getFormEntry(control: string): AbstractControl | null {
        return this.formGroup.get(control)
    }

    constructor(
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private productService: ProductService,
        private router: Router,
        private toastr: ToastrService
    ) {
    }

    ngOnInit(): void {
        const id = this.activatedRoute.snapshot.params['id']
        if (id)
            this.show(id)
    }

    submit(): void {
        const product: ProductInterface = this.formGroup.getRawValue()
        if (this.formGroup.invalid) {
            this.formGroup.markAllAsTouched()
            Object.keys(this.formGroup.controls).map(control => this.getFormEntry(control)?.markAsDirty())
        } else {
            product.id === null
                ? this.store(product)
                : this.update(product)
        }
    }

    private store(product: ProductInterface): void {
        const success = (response: ProductInterface) => {
            this.toastr.info("Produto criado com sucesso!")
            this.formGroup.reset()
            this.router.navigate(['unlogged', 'products', response.id])
        }
        const fail = (error: any) => {
            this.toastr.error("Erro ao criar o produto!")
            console.error(error)
        }
        
        this.productService.store(product).subscribe({
            next: response => success(response),
            error: error => fail(error),
        })
    }

    private update(product: ProductInterface): void {
        const success = (response: ProductInterface) => {
            this.toastr.info("Produto atualizado com sucesso!")
            console.log(response)
            this.formGroup.reset()
            this.router.navigate(['unlogged', 'products', response.id])
        }
        const fail = (error: any) => {
            this.toastr.error("Erro ao atualizar o produto!")
            console.error(error)
        }
        const id: string = String(product.id)
        delete product.id
        this.productService.update(id, product).subscribe({
            next: response => success(response),
            error: error => fail(error),
        })
    }

    private show(id: string): void {
        const success = (product: ProductInterface) => this.formGroup.patchValue({
            id: product.id,
            name: product.name,
            description: product.description,
            value: product.value,
        })

        this.getFormEntry('thumbnail')?.removeValidators(Validators.required)
        const fail = (error: any) => console.error(error)

        this.productService.show(id).subscribe({
            next: response => success(response),
            error: error => fail(error),
        })
    }

    onFileSelect(event: any, element: HTMLElement): void {
        const document: File = event.addedFiles.at(-1)
        this.document = document

        this.parseBase64(document)

        let uploadProgress: number = 0
        const progress: NodeJS.Timer = setInterval(() => {
            if (uploadProgress >= 130) {
                delete element.dataset['uploadProgress']
                clearInterval(progress)
            } else {
                element.dataset['uploadProgress'] = String(uploadProgress++)
            }
        }, 30)

    }

    parseBase64(document: File): void {
        const reader: FileReader = new FileReader
        reader.readAsDataURL(document);
        reader.onload = () => {
            this.getFormEntry('thumbnail')?.setValue(reader.result);
        };
    }
}
