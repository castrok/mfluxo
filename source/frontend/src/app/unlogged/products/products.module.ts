import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        loadChildren: () => import('./products-list/products-list.module').then(m => m.ProductsListModule),
    },
    ...['create', ':id/edit'].map((path: string) => (
        {
            path: path,
            loadChildren: () => import('./products-form/products-form.module').then(m => m.ProductsFormModule),
        }
    )),
    {
        path: ':id',
        loadChildren: () => import('./products-detail/products-detail.module').then(m => m.ProductsDetailModule),
    },
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ]
})
export class ProductsModule {
    constructor() {
        console.log('ProductsModule')
    }
}
