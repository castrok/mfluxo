import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsDetailComponent } from './products-detail.component';
import {RouterModule} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    ProductsDetailComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProductsDetailComponent
            }
        ]),
        ReactiveFormsModule
    ]
})
export class ProductsDetailModule {
  constructor() {
    console.log('ProductsDetailModule')
  }
}
