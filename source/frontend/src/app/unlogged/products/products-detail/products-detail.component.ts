import {Component, OnInit} from '@angular/core';
import ProductInterface from "../../../shared/interfaces/product-interface";
import {ProductService} from "../../../shared/services/product.service";
import {ActivatedRoute, Router} from "@angular/router";
import {thumbnailPath} from "../../../shared/providers/thumbnail-provider";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import CartProductInterface from "../../../shared/interfaces/cart-product-interface";
import {CartProductService} from "../../../shared/services/cart-product.service";
import {subTotal} from "../../../shared/providers/total-cart-product-provider";
import StorageProvider from "../../../shared/providers/storage.provider";
import {ToastrService} from "ngx-toastr";
import CartInterface from "../../../shared/interfaces/cart-interface";


@Component({
    selector: 'mfluxo-products-detail',
    templateUrl: './products-detail.component.html',
    styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements OnInit {

    protected readonly subTotal = subTotal;
    protected readonly thumbnailPath = thumbnailPath;

    product: ProductInterface = Object.assign({})
    cartProduct: CartProductInterface = Object.assign({})

    formGroup: FormGroup = this.formBuilder.group({
        fk_cart: [
            this.cartProduct.fk_cart,
        ],
        quantity: [
            this.cartProduct.quantity,
            {
                validators: [Validators.pattern("^[0-9]*$"), Validators.required],
                asyncValidators: [],
                updateOn: 'change',
            }
        ],
        fk_product: [
            this.cartProduct.fk_product,
        ],
    })

    getFormEntry(control: string): AbstractControl | null {
        return this.formGroup.get(control)
    }

    constructor(
        private productService: ProductService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private cartProductService: CartProductService,
        private storageProvider: StorageProvider,
        private toastr: ToastrService,
        private router: Router
    ) {
    }

    private show(id: string): void {
        const success = (product: ProductInterface) => this.product = product
        const fail = (error: any) => console.error(error)

        this.productService.show(id).subscribe({
            next: response => success(response),
            error: error => fail(error),
        })
    }

    addToCart(): void {
        this.cartProduct = Object.assign({}, this.formGroup.getRawValue())
        const success = (response: CartProductInterface): any => {
            this.toastr.info("Produto adicionado ao carrinho!")
            const cart: CartInterface = this.storageProvider.decriptNdGetLocalStorage('currentCart')
            this.router.navigate(['/unlogged/', 'carts', cart.id])
        }
        const fail = (error: any) => {
            this.toastr.error("Ocorreu um erro ao adicionar o produto ao carrinho!")
            console.error(error)
        }

        if (this.formGroup.valid) {
            this.cartProductService.store(this.cartProduct).subscribe({
                next: response => success(response),
                error: error => fail(error)
            })
        }
        else {
            this.formGroup.markAllAsTouched()
            Object.keys(this.formGroup.controls).map(control => this.getFormEntry(control)?.markAsDirty())
        }

    }

    ngOnInit(): void {
        const id: string = this.activatedRoute.snapshot.params['id']
        const cart: CartInterface = this.storageProvider.decriptNdGetLocalStorage('currentCart')
        this.formGroup.patchValue({
            fk_cart: cart.id,
            fk_product: id
        })
        this.show(id)
    }

    numberOnly(event: any): void {
        const bypassKeys: string[] = [
            'ArrowLeft',
            'ArrowRight',
            'Backspace',
        ]

        if (bypassKeys.indexOf(event.key) === -1 && !event['key'].match("^[0-9]*$"))
            event.preventDefault()
    }
}
