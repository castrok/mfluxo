import {Component, OnInit} from '@angular/core';
import ProductInterface from "../../../shared/interfaces/product-interface";
import {ProductService} from "../../../shared/services/product.service";
import {thumbnailPath} from "../../../shared/providers/thumbnail-provider";

@Component({
    selector: 'mfluxo-products-list',
    templateUrl: './products-list.component.html',
    styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

    protected readonly thumbnailPath = thumbnailPath;
    products: ProductInterface[] | null = []

    constructor(
        private productService: ProductService
    ) { }

    ngOnInit(): void {
        this.index()
    }
    private index(): void {
        const success = (products: ProductInterface[]) => this.products = products
        const fail = (error: any) => console.error(error)

        this.productService.index().subscribe({
            next: products => success(products),
            error: error => fail(error),
        })
    }


}
