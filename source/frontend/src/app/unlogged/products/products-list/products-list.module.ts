import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductsListComponent} from './products-list.component';
import {RouterModule} from "@angular/router";
import {NgxCurrencyModule} from "ngx-currency";

@NgModule({
    declarations: [
        ProductsListComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ProductsListComponent
            }
        ]),
        NgxCurrencyModule
    ]
})
export class ProductsListModule {
    constructor() {
        console.log('ProductsListModule')
    }
}
