import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {RouterModule, RouterOutlet, Routes} from "@angular/router";
import {CurrencyMaskInputMode, NgxCurrencyModule} from "ngx-currency";
import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from "@angular/common";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {LoadingInterceptor} from "./shared/interceptors/loading.interceptor";
import {LoadingComponentModule} from "./shared/components/loading/loading.component.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
registerLocaleData(localePt);

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'unlogged',
    },
    {
        path: 'unlogged',
        loadChildren: () => import('./unlogged/unlogged.module').then(m => m.UnloggedModule),
    }
]

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        RouterOutlet,
        HttpClientModule,
        RouterModule.forRoot(routes),
        NgxCurrencyModule.forRoot({
            align: "left",
            allowNegative: false,
            allowZero: true,
            decimal: ",",
            precision: 2,
            prefix: "R$ ",
            suffix: "",
            thousands: ".",
            nullable: false,
            min: 0.00,
            // max: 0,
            inputMode: CurrencyMaskInputMode.FINANCIAL
        }),
        LoadingComponentModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
    ],
    providers: [
        {
            provide: LOCALE_ID,
            useValue: "pt-BR"
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoadingInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
