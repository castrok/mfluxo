import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {finalize, tap} from 'rxjs/operators';
import {LoadingService} from "../services/loading.service";
import {ToastrService} from "ngx-toastr";


@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  constructor(public loadingService: LoadingService, private toastr: ToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    setTimeout(() => {
      if (!this.loadingService.isLoading) {
        this.loadingService.show();
      }
    }, 300);

    return next.handle(req).pipe(
        finalize(() => {
          setTimeout(() => {
            if (this.loadingService.isLoading) {
              this.loadingService.hide();
            }
          }, 300);
        }),
        tap((event: HttpEvent<any>) => {},
            error => {
              if (error.status === 404)
                this.toastr.error("API indisponível")

              if (error.status === 502)
                this.toastr.info("API em processo de carregamento. Aguarde")
            }
        ))
  }
}
