import {baseURL} from "../../../environments/environment.staging";

export function thumbnailPath(thumbnail: string): string {
        return baseURL + `/thumbnails/${thumbnail}`
    }
