
export function subTotal(quantity: number, value?: number, discount: number = 0): number {
    if (!value || !quantity)
        return 0

    return Number(value - discount) * Number(quantity)
}
