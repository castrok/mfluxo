import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function WhiteSpacesValidator(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {

        const value = control.value;

        if (!value)
            return null;

        return value.replace(/\s/g, '').length === 0 ? { whiteSpaces: true } : null;
    }
}
