import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms"

export function NumericOnlyValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {

        const value = control.value
console.log(value)
        if (!value)
            return null

        return value.toString().match(/^[0-9]?$/) ? {Numeric: true} : null
    }
}
