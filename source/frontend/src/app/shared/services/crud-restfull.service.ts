import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, map, Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export abstract class CrudRestfullService<T> {
  protected constructor(protected base: string, protected httpClient: HttpClient) {  }

  index(): Observable<T[]> {
    return this.httpClient.get<T[]>(this.base).pipe(
        map(response => response),
        catchError(error => throwError(error)),
    )
  }

  show(id: string): Observable<T> {
    const endpoint: string = this.base + `/${id}`
    return this.httpClient.get<T>(endpoint).pipe(
        map(response => response),
        catchError(error => throwError(error)),
    )
  }

  store(payload: T): Observable<T> {
    return this.httpClient.post<T>(this.base, payload).pipe(
        map(response => response),
        catchError(error => throwError(error)),
    )
  }

  update(id: string, payload: T): Observable<T> {
    const endpoint: string = this.base + `/${id}`
    return this.httpClient.patch<T>(endpoint, payload).pipe(
        map(response => response),
        catchError(error => throwError(error)),
    )
  }

  delete(id: string): Observable<T> {
    const endpoint: string = this.base + `/${id}`
    return this.httpClient.delete<T>(endpoint).pipe(
        map(response => response),
        catchError(error => throwError(error)),
    )
  }
}
