import {inject, Injectable} from '@angular/core';
import {CrudRestfullService} from "./crud-restfull.service";
import CartProductInterface from "../interfaces/cart-product-interface";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment.staging";

@Injectable({
  providedIn: 'root',
})
export class CartProductService extends CrudRestfullService<CartProductInterface> {

  constructor() {
    super(environment.shopping, inject(HttpClient));
  }

}
