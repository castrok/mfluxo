import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import CartInterface from "../interfaces/cart-interface";
import {environment} from "../../../environments/environment.staging";
import {CrudRestfullService} from "./crud-restfull.service";

@Injectable({
  providedIn: 'root'
})
export class CartService extends CrudRestfullService<CartInterface>{
  constructor() {
    super(environment.carts, inject(HttpClient));
  }
}
