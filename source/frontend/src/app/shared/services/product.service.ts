import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, throwError} from "rxjs";
import ProductInterface from "../interfaces/product-interface";
import {environment} from "../../../environments/environment.staging";
import {CrudRestfullService} from "./crud-restfull.service";

@Injectable({
  providedIn: 'root'
})
export class ProductService extends CrudRestfullService<ProductInterface> {
  constructor() {
    super(environment.products, inject(HttpClient));
  }
}
