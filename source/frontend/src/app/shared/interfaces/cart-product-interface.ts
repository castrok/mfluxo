import ProductInterface from "./product-interface";

export default interface CartProductInterface<T = string> {
    id?: T
    fk_cart: T
    fk_product: T
    quantity: number
    created_at?: Date
    product?: ProductInterface
}
