export default interface ProductInterface<T = string> {
    id?: T
    created_at?: Date
    description?: string
    name: string
    thumbnail: string
    updated_at?: Date
    value: number
}
