import ProductInterface from "./product-interface";

export default interface CartInterface<T = string> {
    id?: T
    name: string
    situation?: 'BUYING' | 'CLOSED'
    created_at?: Date
    products?: ProductInterface[]
}
