import { Component, AfterViewInit } from '@angular/core';
import {LoadingService} from "../../services/loading.service";


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: [ './loading.component.scss' ]
})
export class LoadingComponent implements AfterViewInit {

  public loading = false;

  constructor(public loadingService: LoadingService) { }

  ngAfterViewInit() {

    // setTimeout(() => {
      this.loadingService.loading.subscribe(loading => {
        this.loading = loading;
      });
    // }, 200);

  }

}
