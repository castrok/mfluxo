import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import {RouterLink, RouterLinkActive} from "@angular/router";
import {MatTooltipModule} from "@angular/material/tooltip";



@NgModule({
    declarations: [
        NavbarComponent
    ],
    exports: [
        NavbarComponent
    ],
    imports: [
        CommonModule,
        RouterLink,
        RouterLinkActive,
        MatTooltipModule
    ]
})
export class NavbarModule { }
