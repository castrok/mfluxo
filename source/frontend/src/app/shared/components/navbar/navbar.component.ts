import { Component } from '@angular/core';
import StorageProvider from "../../providers/storage.provider";
import CartInterface from "../../interfaces/cart-interface";
import {Router} from "@angular/router";

@Component({
  selector: 'mfluxo-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private storageProvider: StorageProvider, private router: Router) {
  }

  redirectToCart() {
    const cart: CartInterface = this.storageProvider.decriptNdGetLocalStorage('currentCart') || null
    this.router.navigateByUrl(`/unlogged/carts/${cart?.id || 'create'}`)
  }

}
