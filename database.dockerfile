FROM postgres:12
ADD psql_dump.sql /docker-entrypoint-initdb.d/psql_dump.sql
EXPOSE 5432
