FROM nginx:stable
RUN apt update -y

WORKDIR /var/www/html/
ADD /conf/balancer.conf /etc/nginx/conf.d/default.conf
EXPOSE 443
