FROM node:16.20-slim as mfluxo_frontend_build

WORKDIR /var/www/html/
ADD /source/frontend .

RUN apt update \
&& apt install --no-install-recommends -y \
&& npm install \
&& npm run build -c staging

FROM nginx:stable
WORKDIR /usr/share/nginx/html/
RUN rm -rf *
COPY --from=mfluxo_frontend_build /var/www/html/dist/ /usr/share/nginx/html
ADD /conf/frontend.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
