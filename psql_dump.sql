create schema mfluxo;
SET search_path = mfluxo;

create table if not exists products
(
    id          serial constraint product_pkey primary key,
    name        varchar unique not null,
    description text                        default null,
    value       decimal(10, 2) not null     default 0,
    thumbnail   varchar        not null,
    created_at  timestamp without time zone default now(),
    updated_at  timestamp without time zone default null
);

create table if not exists carts
(
    id         serial constraint cart_pkey primary key,
    name        varchar unique default null,
    situation  varchar not null CHECK (situation IN ('BUYING', 'CLOSED')),
    created_at timestamp without time zone default now(),
    updated_at timestamp without time zone default null
);

create table if not exists cart_products
(
    id         serial
        constraint cart_product_pkey primary key,
    fk_cart    int     not null,
    quantity   integer not null,
    fk_product int     not null,
    created_at timestamp without time zone default now(),

    CONSTRAINT fk_cart_products_cart
        FOREIGN KEY (fk_cart)
            REFERENCES carts(id),

    CONSTRAINT fk_cart_products_product
        FOREIGN KEY (fk_product)
            REFERENCES products(id)
);
