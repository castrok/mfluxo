FROM nginx:stable
RUN apt update -y

WORKDIR /var/www/html/
ADD /conf/backend.conf /etc/nginx/conf.d/default.conf

EXPOSE 443
