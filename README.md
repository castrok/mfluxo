# Teste técnico fullstack developer

## Sobre este projeto
Este é um teste técnico para a vaga de desenvolvedor fullstack utilizando as tecnologias **[Angular](https://angular.io/)**, **[Laravel](https://laravel.com/)** e **[Docker](https://www.docker.com/)**.
Para subir o projeto basta executar o comando <code>docker-compose up -d --build</code> na raiz do projeto.

## Funcionalidades
- CRUD carrinhos de compras
- CRUD produtos
- Fechamento de venda

## Pré requisitos
Para executar este projeto é necessário que tenha instalado e pré configurado
- **[Docker](https://docs.docker.com/desktop/)**
- **[Docker Compose](https://docs.docker.com/compose/gettingstarted/)**
- A porta <code>::2023</code> deve estar disponível no dispositivo host pois o projeto sera servido nela. Caso contrário o a construção dos containeres irá falhar.

## Considerações
- Não é neessário nenhuma configuração adicional nos arquivos do projeto.
- Todo o ambiente estará pronto para uso após a finalização da construção dos conteineres.
- Apos finalizar o processo de construção dos containeres o sistema estará disponível em http://localhost:2023

## Pontos de atenção
- Em certos dispositivos, pode ser que após subir os containeres os mesmos ainda estejam finalizando configurações internas o que pode levar alguns minutos para estarem disponíveis para uso.
Recomenda-se aguardar alguns minutos e tentar acessar novamente o projeto através do pelo navegador no link http://localhost:2023.

- Ao acessar o sistema é neesário criar ao menos um carrinho de compras, caso contrário o sistema poderá apresentar falhas.


## Licenciamento
**[GPL3](https://github.com/IQAndreas/markdown-licenses/blob/master/gnu-gpl-v3.0.md)**
